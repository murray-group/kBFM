clear;
%cmd=['set path=%path:C:\Program Files\MATLAB\R2018b\bin\win64;=% & bin\Release\kBFM.exe input_87_22_22_circular.txt'];
cmd=['set path=%path:C:\Program Files\MATLAB\R2020b\bin\win64;=% & bin\Release\kBFM.exe input_150_150_150_k7_linear.txt'];
tic;
[status, cmdout]=system(cmd,'-echo');%outputs to current matlab directory
toc;
%%
file=fopen('output.txt');
tline=fgetl(file);

    z=textscan(tline,'%f','Delimiter','\t');%outputs cell array
    Nx=z{1}(1);
    Ny=z{1}(2);
    Nz=z{1}(3);
    tline=fgetl(file);
i=1;
while ischar(tline)
    %z=textscan(tline,'%f','Delimiter','\t');%outputs cell array
    [z,pos]=textscan(tline,'%f',1,'Delimiter','\t');%outputs cell array
    t(i)=z{1}(1);
    [z,pos1]=textscan(tline(pos+1:end),'%u32',1,'Delimiter','\t');%outputs cell array
    n_monomers_bridged(i)=z{1}(1);
    z=textscan(tline(pos+pos1+1:end),'%u32','Delimiter','\t');%outputs cell array
    configs(i,:)=z{1}(1:end)';
    tline=fgetl(file);
    i=i+1;
end
fclose(file);
bridges=load('outputbridges.txt');
%save('polymer_p20_bridging20_kun1.mat','Nx','Ny','Nz','t','configs','n_monomers_bridged','bridges')

%%
get_coord =@(ind) [floor(double(ind)/(Ny*Nz))+1;floor(mod(double(ind),Ny*Nz)/Nz)+1; mod(mod(double(ind),Ny*Nz),Nz)+1];


%% display polymer and bridges
figure(2)

ind=1000;

N=size(configs,2);

p=configs(ind,:);
clear P;
for i=1:N
P(i,:)=get_coord(p(i));
end
x=P(:,1);
y=P(:,2);
z=P(:,3);

c=parula(256);
h=colormapline(x,y,z,c);
%tubeplot([x';y';z'],0.2)
set(h,'linewidth',4)
axis equal;
xlim([0,Nx-1])
ylim([0,Ny-1])
zlim([0,Nz-1])
box on;
grid on;
ax=gca;
ax.BoxStyle = 'full';

hold on;
for i=1:430
    if bridges(ind,i)>0
        I=bridges(ind,i);
        if I>i+4
            plot3([x(i);x(I)],[y(i);y(I)],[z(i);z(I)],'-r','LineWidth',2) %genomically distant bridges in red
        elseif I>i
           plot3([x(i);x(I)],[y(i);y(I)],[z(i);z(I)],'-k','LineWidth',2)
        end
    end
end
hold off;

%% Calculate the Space Filling Factor


for ind=1:101
p=configs(ind,:);
clear P;
for i=1:N
P(i,:)=get_coord(p(i));
end
x=P(:,1);
y=P(:,2);
z=P(:,3);
img=zeros(Nx,Ny,Nz);
for i=1:N
img(x(i),y(i),z(i))=1;
end
%for i=1:12
SE = strel('cube',3);
img_d=imdilate(img, SE);
sff_b(ind)=sum(img_d(:)==1)/(Nx*Ny*Nz);
%end
end

figure(10)
plot(t,sff_b)


%% Genomic distance between bridges

x=1:N;
bridges(bridges==0)=NaN;
gd=abs(bridges-x);%for linear polymer

figure(2)
histogram(gd,[0.5:1:115])
xlabel('Genomic distance between bridges')

%% distance and proximity maps
figure(3)
clf;

range=580:1000;

x=(-N/2+1):(N/2);

meand_vs_gd=0;
H=zeros(N,N);


for k=range
    for i=1:N
        P(i,:)=get_coord(configs(k,i));
    end
    d=squareform(pdist(P));% distance matrix, entry (i,j) is distance between monomer i and monomer j
    ds=NaN(N,N);
    for j=1:N
    ds(j,[j:N]-j+1)=d(j,j:N);
    end
    meand_vs_gd=meand_vs_gd+nanmean(ds);
    H=H+d;
end

meand_vs_gd=meand_vs_gd/length(range);
H=H/length(range);

subplot(2,1,1)
%H=squareform(pdist(P));
imagesc(H);
ax=gca;
ax.YDir='reverse';
colorbar;
axis equal;
xlim([1,size(configs,2)]);
title('Distance Map')


subplot(2,1,2)
HiC=zeros(size(configs,2),size(configs,2));
for k=range
for i=1:length(p)
P(i,:)=get_coord(configs(k,i));
end
HiC=HiC+double(squareform(pdist(P([1:end],:)))<3);    
end
HiC=HiC/length(range);
%Probability of being within the above spatial distance as a function of
%genomic distance
HiCs=NaN(N,N);
for j=1:N
    HiCs(j,[j:N]-j+1)=HiC(j,j:N);
end
Pcontact=nanmean(HiCs);

subplot(2,1,2)
%H=squareform(pdist(P));
imagesc(HiC);
ax=gca;
ax.YDir='reverse';
colorbar;
axis equal;
xlim([1,size(configs,2)]);
title('Proximity Map')
%% histogram of all inter-monomer distances
figure(4)
subplot(3,1,1)
histogram(H)
xlabel('inter-monomer distance')

% spatial distance vs genomic distance

subplot(3,1,2)
plot(0:(N-1),meand_vs_gd)
xlabel('genomic distance')
ylabel('spatial distance')

%Probability of being in contact

subplot(3,1,3)
plot(0:(N-1),Pcontact)
xlabel('genomic distance')
ylabel('Probability of being in contact')

%% x-Position and x-velocity autocorrelation

x=floor(double(configs)/(Ny*Nz));
%y=floor(double(mod(configs,Ny*Nz))/Nz);

dt=t(2)-t(1);

figure(3)
clf;
subplot(2,1,1)

c=[];
for i=1:size(configs,2)
[c(:,i),lags]=xcov(x(:,i),'coeff');
end

I=lags>=0;
c=c(I,:);
c_avg=mean(c,2)';
lags=lags(I);
plot(lags,c_avg,lags,c(:,10))
xlim([0,lags(end)]);
hold on;
line([0,lags(end)],[0,0])
hold off;
xlabel('Lag (timesteps)');
ylabel('x-position auto-correlation')

M=63;
sum(c_avg(1:M))


subplot(2,1,2)
hold on
for k=1:5:50%number of timesteps over which to calculate the velocity
v=(x(k+1:end,:)-x(1:(end-k),:))/(k*dt);

cv=[];
for i=1:size(configs,2)
[cv(:,i),lags]=xcorr(v(:,i),'coeff');
end

I=lags>=0;
cv=cv(I,:);
cv_avg=mean(cv,2)';
lags=lags(I);
plot(lags/(k),cv_avg)
end
xlim([0,10]);
%hold on;
line([0,10],[0,0])
hold off;
xlabel('Lag/delta');
ylabel('x-velocity auto-correlation')

% figure(3)
% clf;
% autocorr(floor(configs(:,i)/(Ny*Nz)),'NumLags',100)
%% Autocorrelation of the bond angle, Zhang et al 2009, Polymers
clear bcor;

for i=1:size(configs,1)
    b=[diff(get_coord(configs(i,:)),1,2)];% bond vectors
    bond_length(i)=mean(vecnorm(b));
for s=1:(length(b)-1)/2
    bcor(i,s)=mean(dot(b(:,1:end-s),b(:,[s+1:end])))/bond_length(i)^2;%<r_i,r_i+s>/<l>^2 // for each config and distance s, this is the mean of the dot products divided by the square of the mean bond length  
end
    mean_cos(i)=mean(dot(b(:,1:end-1),b(:,[2:end]))./vecnorm(b(:,1:end-1))./vecnorm(b(:,2:end))); % for each config this is the mean of the cos(theta) between consecutive bonds (s=1)
end
figure(11)
plot(mean(bcor))
disp(['The persistence length is ', num2str(mean(-bond_length./log(mean_cos))),' lattice units = ',num2str(num2str(mean(-1./log(mean_cos)))),' bonds']);%estimate based only on lag 1 rather than fitting the autocorrelation to an exponential

figure(12)
plot(t,-bond_length./log(mean_cos))
%% Example trajectory and Histogram of dx
figure(4)

x=floor(double(configs)/(Ny*Nz));
plot(t,x(:,200))
ylim([1,Nx])

figure(5)
dx=diff(x,1);
histogram(dx);
dt=t(2)-t(1);
D=var(dx(:))/2/dt*(3/Nx)^2
%% MSD and other dynamic measures

w=10; %number of monomers to take the centroid of
trange=[50:10:500,550:50:5000];
%trange=[1:1000];
CoM=mean(x,2);

CoMSD=NaN(1,length(trange));
MSD=NaN(1,length(trange));
MSDfromCOM=NaN(1,length(trange));
monomer_range=30:401;
for k=1:length(trange)
    clear dx;
    j=1;
    for i=monomer_range
        dx(:,j)=mean(x(trange(k)+1:end,i:(i+w-1)),2)-mean(x(1:(end-trange(k)),i:(i+w-1)),2);
        j=j+1;
    end
    %dx=x(trange(k)+1:end,monomer_range)-x(1:(end-trange(k)),monomer_range);
    dCoM=CoM(trange(k)+1:end)-CoM(1:(end-trange(k)));
    CoMSD(k)=mean(dCoM.^2);
    MSD(k)=mean(dx(:).^2);
    dxCoM=bsxfun(@minus,dx,dCoM);
    MSDfromCOM(k)=mean(dxCoM(:).^2);
end

Rg=mean(bsxfun(@minus,x,CoM).^2,2);


figure(9)
clf;
subplot(6,1,1:4)
loglog(trange*dt,MSD,trange*dt,1000*CoMSD,trange*dt,MSDfromCOM,trange*dt,2*(trange*dt).^0.38,'--k',trange*dt,3*(trange*dt).^0.5,'--k',trange*dt,(trange*dt).^1,'.-k')
legend('MSD','1000*CoMSD','MSDfromCOM','\alpha=0.5','\alpha=0.38','\alpha=1')
xlabel('t')
ylabel('x-MSD (lattice sites^2)')

fun=@(a,t) a(1)+a(2)*t;
[beta,r,~,cov,MSE]=nlinfit(log10(dt*trange),log10(MSD),fun,[-2.5,0.5]);
ci = nlparci(beta,r,'covar',cov);
%disp(['beta(1) = ',num2str(beta(1)),' with 95% CI [',num2str(ci(1,1)),', ',num2str(ci(1,2)),']'])
disp(['beta(2) = ',num2str(beta(2)),' with 95% CI [',num2str(ci(2,1)),', ',num2str(ci(2,2)),']'])
hold on;
loglog(trange*dt,10^beta(1)*(trange*dt).^beta(2),'--g','linewidth',1);
hold off;

subplot(6,1,5)
plot(t,Rg)
xlabel('t')
ylabel('<Rg^2>')

subplot(6,1,6)
plot(t,n_monomers_bridged)
xlabel('t')
ylabel('Number of monomers bridged')



