#include "Unbridge.h"

unsigned long long int Unbridge::totalcount=0;


double Unbridge::rate(const Polymer& P)
{
    return rate_const*P.n_bridged_monomers;
}

void Unbridge::do_reaction(Polymer& P, double rx_u, double rate)
{
    assert(rate=this->rate(P));

    int n=rx_u/rate_const;//rounds down by default

    int i=0;
    int counter=0;
    while(counter<=n){

        if(P.bridges[i]>0) counter++;
        i++;
    }
    //i-1 is the nth monomer

    int n2=P.bridges[i-1]-1;//the other monomer

    P.bridges[i-1]=0;
    P.bridges[n2]=0;
    P.n_bridged_monomers=P.n_bridged_monomers-2;

    reactionsAffected.resize(3);
    reactionsAffected[0]=i-1;
    reactionsAffected[1]=n2;
    reactionsAffected[2]=P.monomer_loc.size();//unbridging

    totalcount++;

    return;

}


