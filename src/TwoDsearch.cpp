#include "TwoDsearch.h"


TwoDsearch::TwoDsearch(size_t n_react): L(ceil(sqrt(n_react))), totalsum(0.0),errorupperboundtotalsum(0.0)
{
    M.resize(L,std::vector<double>(L,0.0));// LxL matrix
    rowsums.resize(L,0);
    errorupperboundrows.resize(L,0.0);
    //std::cout << double_precision <<'\n';
}

//TwoDsearch::TwoDsearch(const std::vector<std::vector<size_t>> reactionlists):L(0),totalsum(0)
//{
//    size_t nLists=reactionlists.size();
//    matrices.resize(nLists);
//    size_t n_react=0;
//
//    for(size_t i=0; i< nLists;i++){
//        n_react+=reactionlists[i].size();
//    }
//    L=ceil(sqrt(n_react));
//    M.resize(L,std::vector<double>(L,0));// LxL matrix
//    rowsums.resize(L,0);
//
//}


TwoDsearch::~TwoDsearch()
{
    //dtor
}


std::tuple<size_t,double, double> TwoDsearch::search_obj_search(double rx_u) const
{
    //first linear search on matrices



    //first linear search on row sums
    size_t i=0;
//    if(rx_u > totalsum ){
//
//    std::cout << rx_u << '\t' << totalsum << '\n';
//    }

    assert(rx_u <= totalsum );
    while(rx_u>rowsums[i])
    {
        rx_u-=rowsums[i];
        ++i;
        assert(i<rowsums.size());
    }


    //then linear search along the row
    size_t j=0;
    while(rx_u>M[i][j])
    {
        rx_u-=M[i][j];
        ++j;
        assert(j<rowsums.size());
    }
    assert(rx_u<=M[i][j]);


    return std::make_tuple(L*i+j,rx_u,M[i][j]);//reaction index, scaled random number (between 0 and propensity), propensity

}


void TwoDsearch::update_search_obj(size_t rx,double rate)
{
    //size_t j=rx%L;
    size_t i=size_t(float(rx)/L);//truncates, same as floor for positive numbers but faster
    size_t j=rx-i*L;
    assert(i<L);
    assert(j<L);
    assert(!std::signbit(rate));//assert positive
    //std::cout << i << '\t' << rowsums[i] << '\t' << M[i][j] << '\t' << rate << '\n';
    //assert(rowsums[i]>=M[i][j]);

    errorupperboundrows[i]+=((rowsums[i]+M[i][j]+rate)*double_precision);
    double rowsumold=rowsums[i];


    if(errorupperboundrows[i]>2.0e-10*rowsums[i])
    {
        M[i][j]=rate;
        //std::cout <<"resumming rows "<< '\n';
        rowsums[i]=std::accumulate(M[i].begin(), M[i].end(), 0.0);
        errorupperboundrows[i]=0.0;

        //totalsum=std::accumulate(rowsums.begin(), rowsums.end(), 0.0);
        // errorupperboundtotalsum=0.0;
    }
    else
    {
        rowsums[i]+=(-M[i][j]+rate);
        M[i][j]=rate;
    }
    if(std::signbit(rowsums[i])) //if negative
    {
        rowsums[i]=0.0;
    }

    errorupperboundtotalsum+=((totalsum+rowsumold+rowsums[i])*double_precision);

    if(errorupperboundtotalsum>2.0e-10*totalsum)
    {
        //std::cout <<"resumming total "<< '\n';
        totalsum=std::accumulate(rowsums.begin(), rowsums.end(), 0.0);
        errorupperboundtotalsum=0.0;
    }
    else
    {
        totalsum+=(-rowsumold+rowsums[i]);
    }


    if(std::signbit(totalsum)) //if negative
    {
        totalsum=0.0;
        //std::cout<< totalsum << '\t'<<totalsumold << '\t'<< rowsumold << '\t'<< rowsums[0] <<'\t'<<rowsums[1] <<'\n';
    }
    assert(!std::signbit(totalsum));



}


double TwoDsearch::get_root_value() const
{
    return totalsum;
}

