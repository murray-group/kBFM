//******************************************************
//	Implement a class that does Gillespie simulations

# include <vector>
# include <algorithm>
# include <cassert>

# include "GillespieSimulation.h"
//______________________________________________________

GillespieSimulation::GillespieSimulation(
    const ReactionsArray &RX,
    Polymer &P
)
//******************************************************
//	Build a GillespieSimulation with all its
// associated moving parts.
    : reactions(RX), P(P),SimTime(0),n_react(RX.size()),rate_sum(0),search_obj(new Search_obj(n_react)),
      seed(std::chrono::system_clock::now().time_since_epoch().count()),gen(seed), unidist(0.0,1.0),
      jsr_value(123456789*seed), jcong_value(234567891*seed),w_value(345678912*seed),z_value(456789123*seed),n_comp(P.monomer_loc.size())
//
{


    rate.resize(n_react);
    //std::cout<< "Number of reactions: " << n_react <<'\n';

    update_rates();
    //std::cout<< "Update tree for first time" <<'\n';


//  Call ZIGSET to set the seed.
    zigset ( jsr_value, jcong_value, w_value, z_value );
    //std::cout << "Seed: "<< seed << '\n';
}
//______________________________________________________


double GillespieSimulation::advance_time( double t_stop )
//****************************************************
//	Do events until the specified time is reached.
// If we're at equilibrium this doesn't require any
// real action on our part.
//
{
    //if( !is_stopped() ) {
    double	time = 0.0 ;
    double	dt = time_to_next_event() ;
    while( ((time + dt) <= t_stop) ) //&& !is_stopped() ) {
    {
        time += dt ;
        SimTime+=dt;
        //auto rx_n = choose_next_reaction() ;
        perform_reaction(choose_next_reaction()) ;
        dt = time_to_next_event() ;
    }
    return time;
    //}
}

double GillespieSimulation::advance_step( size_t n_steps )
//advance a number of steps (reaction events)
{
    size_t steps=0;
    double	time = 0.0 ;
    double	dt = time_to_next_event() ;
    while(steps<n_steps ) //&& !is_stopped() ) {
    {
        time += dt ;
        steps++;
        //auto rx_n = choose_next_reaction() ;
        perform_reaction(choose_next_reaction()) ;
        dt = time_to_next_event() ;
    }
    return time;

}

//______________________________________________________

double GillespieSimulation::time_to_next_event( void )
//*****************************************************
//	Draw a time from the exponential distribution
// with mean 1/R, where R is the sum of all the rates.
{
    double dt = 0.0 ;
    dt=(1.0/rate_sum)*r4_exp_value( );//uses ziggurat
    return( dt ) ;
}
//______________________________________________________



std::tuple<size_t,double,double> GillespieSimulation::choose_next_reaction( void )
//*******************************************************
//	Given that a reaction has happened, choose which one.
// This is analagous to drawing a value from a discrete
// distribution.
//
{
    // Get a number drawn uniformly from [0, R), where
    // R is the sum of all the rates.
    double R = rate_sum ;
    assert(R==search_obj->get_root_value());
    //double rx_u = R * r4_uni_value();
    double rx_u = R * unidist(gen);
    auto rx_n=search_obj->search_obj_search(rx_u);
    assert(std::get<0>(rx_n) < n_react);
    //std::cout << rx_num << '\n';
    return rx_n;
}
//______________________________________________________

void GillespieSimulation::perform_reaction( std::tuple<size_t, double,double> rx_n )
//****************************************************
//	This is the main engine of the dynamics.
{
    //std::cout << rx_num << '\t' << rate[rx_num] << '\n';
    // Debugging paranoia
    auto rx_num=std::get<0>(rx_n);
    assert( rx_num < n_react ) ;
    assert( rx_num < rate.size() ) ;
    assert( rate[rx_num] > 0.0 ) ;

    // Do the reaction

    //auto n=std::make_tuple( std::get<1>(rx_n),std::get<2>(rx_n));
    reactions[rx_num]->do_reaction(P, std::get<1>(rx_n), std::get<2>(rx_n)) ;

    // Recompute the rates and their sums
    update_rates(rx_num); //also updates rate_sum
}
//______________________________________________________

void GillespieSimulation::update_rates( void )//calculate all reaction rates
//****************************************************
//	Recompute all rates.
{
    // Debugging paranoia
    assert( n_react > 0 ) ;
    assert( rate.size() == n_react ) ;
    //assert( rate_sum.size() == reactions.size() ) ;
    for(size_t i=0; i<n_react; ++i)
    {
        rate[i] = reactions[i]->rate(P);
        assert(rate[i]>=0);
        search_obj->update_search_obj(i,rate[i]);
    }

    rate_sum=search_obj->get_root_value();
    std::cout << "rate sum   "<< rate_sum << '\n';
    assert(rate_sum>=0);
}

void GillespieSimulation::update_rates(std::size_t rx_num) // re-calculate only the affected rates and partial sums
//****************************************************
//	Recompute all rates.
{
    // Debugging paranoia
    assert( n_react > 0 ) ;
    assert( rate.size() == n_react ) ;
    assert( rx_num < n_react) ;

    std::vector<size_t> &rxs=reactions[rx_num]->reactionsAffected;//reference variable

    //update the rates of these reactions
    for(const size_t rx : rxs)
    {
        assert(rx<n_react);
        rate[rx] = reactions[rx]-> rate(P);
        assert(rate[rx]>=0);
        //std::cout<< "updating monomer " << rx << " at (" << P.monomer_loc3[rx][0] << ", " << P.monomer_loc3[rx][1] << ", " << P.monomer_loc3[rx][2] << ")" <<'\n';
        search_obj->update_search_obj(rx,rate[rx]);
    }

    if(typeid(*reactions[rx_num])==typeid(Move)){
    auto ptr{std::dynamic_pointer_cast<Move>(reactions[rx_num])};
    std::vector<std::pair<size_t,int>> &rxs=ptr->non_neighbour_monomers_affected;
    for(const auto rx : rxs)
    {
        assert(rx.second<7);
        assert(rx.second>-7);
        auto ptr2{std::dynamic_pointer_cast<Move>(reactions[rx.first])};
        rate[rx.first] = ptr2-> rate(P,rx.second);
        assert(rate[rx.first]>=0);
        //std::cout<< "updating monomer " << rx.first << " at (" << P.monomer_loc3[rx.first][0] << ", " << P.monomer_loc3[rx.first][1] << ", " << P.monomer_loc3[rx.first][2] << ") dir2: " << rx.second <<'\n';
        search_obj->update_search_obj(rx.first,rate[rx.first]);
    }
    }

    rate_sum=search_obj->get_root_value();
    //std::cout << rate_sum << '\n';
    assert(rate_sum>=0);
}


void GillespieSimulation::update_rates_only(std::size_t rx) //update the search obj if reaction rate constants have been changed
{
    rate[rx] = reactions[rx]-> rate(P);
    assert(rate[rx]>=0);
    search_obj->update_search_obj(rx,rate[rx]);
    rate_sum=search_obj->get_root_value();
}




