#include "Move.h"



unsigned long long int Move::totalcount=0;



Move::~Move()
{
    //dtor
}


double Move::rate(const Polymer& P)
{
    //std::cout << "rate0" << '\t';
    find_allowable_moves(P);
    double rate=0.0;
    for(int i=0; i<6; ++i)
    {
        //std::cout << allowable_moves[i] << '\t';
        rate+=move_rates[i];
    }
    //std::cout <<"rate1" <<'\n';
    return rate*rate_const;
}


double Move::rate(const Polymer& P,int dir2)
{
    //std::cout << "rate0" << '\t';
    update_allowed_move(P,dir2);
    double rate=0.0;
    for(int i=0; i<6; ++i)
    {
        //std::cout << allowable_moves[i] << '\t';
        rate+=move_rates[i];
    }
    //std::cout <<"rate1" <<'\n';
    return rate*rate_const;
}

void Move::do_reaction(Polymer& P, double rx_u, double rate)
{
    double r= this->rate(P) ;
    //std::cout << "attempting to move monomer " << n << " at " << '(' << P.monomer_loc3[n][0] << ", " << P.monomer_loc3[n][1] << ", " << P.monomer_loc3[n][2] << ")\t" << rate << '\t' << r<<'\n';
    assert(rate==r);

    //std::cout <<"reaction0" <<'\n';
    double m=rx_u/rate_const;//rounds down by default
    assert(m<6);
    int i=0;
    double counter=0.0;
    while(counter<=m)
    {
        counter+=move_rates[i];
        i++;
    }//direction is 'i-1'th entry in allowable moves
    assert(i-1<6);
    int axis=(i-1)/2;
    //std::cout << rx_u << '\t' << rate_const <<'\t' << axis <<'\n';
    int dir=2*((i-1)%2)-1;
    assert(dir==-1 || dir==1);
    lattice_index_type loc_old=P.monomer_loc[n];//current location

    std::array<int, 3> &ind=P.monomer_loc3[n];
    int x=ind[0];
    int y=ind[1];
    int z=ind[2];

    lattice_index_type loc_new;

    switch(axis)
    {
    case 0:
    {
        loc_new=P.lattice.get_single_index(x+dir,y,z);
        P.monomer_loc3[n]={x+dir,y,z};
        break;
    }
    case 1:
    {
        loc_new=P.lattice.get_single_index(x,y+dir,z);
        P.monomer_loc3[n]={x,y+dir,z};
        break;
    }
    case 2:
    {
        loc_new=P.lattice.get_single_index(x,y,z+dir);
        P.monomer_loc3[n]={x,y,z+dir};
        break;
    }
    default:
        std::cout<<"error in move:do_reaction" << '\n';
    }

    if(loc_new==0){
    std::cout << "Location 0 selected \n";
    }

    assert(P.lattice.get_monomer(loc_old)==n+1);

    P.lattice.set_monomer(loc_old,0);
    assert(P.lattice.get_monomer(loc_new)==0);

    P.lattice.set_monomer(loc_new,n+1);
    P.monomer_loc[n]=loc_new;

    totalcount++;

//    auto f=[P](int ind) {auto p=P.lattice.get_indices(ind); std::cout<<"("<<p[0]<<','<<p[1]<<','<<p[2]<<')';};
//    std::cout << "momomer " << n <<" moved from " << loc_old;
//    f(loc_old);
//    std::cout << " to " << loc_new;
//    f(loc_new);
//    std::cout << '\n';

    update_non_neighbour_monomers_affected(x,y,z,axis,dir,P);
    //std::cout << '\t'<< non_neighbour_monomers_affected.size() << " non-neighbour monomers affected" << '\n';

}


void Move::find_allowable_moves(const Polymer &P)
{
    const  std::array<int, 3> &pos=P.monomer_loc3[n];
    const int x=pos[0];
    const int y=pos[1];
    const int z=pos[2];

    size_t i=0;

    bool is_bridged=P.bridges[n];

// allow bridged bonds to move as long as the bridge length does not exceed a certain length
    allowable_moves[0]=is_bridged ? check_bridge_length({x-1,y,z},P.monomer_loc3[P.bridges[n]-1]):1;
    allowable_moves[1]=is_bridged ? check_bridge_length({x+1,y,z},P.monomer_loc3[P.bridges[n]-1]):1;
    allowable_moves[2]=is_bridged ? check_bridge_length({x,y-1,z},P.monomer_loc3[P.bridges[n]-1]):1;
    allowable_moves[3]=is_bridged ? check_bridge_length({x,y+1,z},P.monomer_loc3[P.bridges[n]-1]):1;
    allowable_moves[4]=is_bridged ? check_bridge_length({x,y,z-1},P.monomer_loc3[P.bridges[n]-1]):1;
    allowable_moves[5]=is_bridged ? check_bridge_length({x,y,z+1},P.monomer_loc3[P.bridges[n]-1]):1;

//    allowable_moves[0]=!is_bridged;
//    allowable_moves[1]=!is_bridged;
//    allowable_moves[2]=!is_bridged;
//    allowable_moves[3]=!is_bridged;
//    allowable_moves[4]=!is_bridged;
//    allowable_moves[5]=!is_bridged;

    while(i<n_neigh){

    const std::array<int, 3> &pos_neigh=P.monomer_loc3[P.monomer_neighbours[n][i]];
//    const int x0(pos_neigh[0]);
//    const int y0(pos_neigh[1]);
//    const int z0(pos_neigh[2]);

    allowable_moves[0]=allowable_moves[0] && check_length({x-1,y,z},pos_neigh);//the new position is on the lattice because of is_XX_plane_free below checks that x-2 is in [0,Nx-1]
    allowable_moves[1]=allowable_moves[1] && check_length({x+1,y,z},pos_neigh);
    allowable_moves[2]=allowable_moves[2] && check_length({x,y-1,z},pos_neigh);
    allowable_moves[3]=allowable_moves[3] && check_length({x,y+1,z},pos_neigh);
    allowable_moves[4]=allowable_moves[4] && check_length({x,y,z-1},pos_neigh);
    allowable_moves[5]=allowable_moves[5] && check_length({x,y,z+1},pos_neigh);

//    allowable_moves[0]=allowable_moves[0] && check_length_d(x-1-x0,y-y0,z-z0);//the new position is on the lattice because of is_XX_plane_free below checks that x-2 is in [0,Nx-1]
//    allowable_moves[1]=allowable_moves[1] && check_length_d(x+1-x0,y-y0,z-z0);
//    allowable_moves[2]=allowable_moves[2] && check_length_d(x-x0,y-1-y0,z-z0);
//    allowable_moves[3]=allowable_moves[3] && check_length_d(x-x0,y+1-y0,z-z0);
//    allowable_moves[4]=allowable_moves[4] && check_length_d(x-x0,y-y0,z-1-z0);
//    allowable_moves[5]=allowable_moves[5] && check_length_d(x-x0,y-y0,z+1-z0);
    i++;
    }

    allowable_moves[0]=allowable_moves[0] && P.lattice.is_yz_plane_free(x-2,y,z);
    allowable_moves[1]=allowable_moves[1] && P.lattice.is_yz_plane_free(x+2,y,z);
    allowable_moves[2]=allowable_moves[2] && P.lattice.is_xz_plane_free(x,y-2,z);
    allowable_moves[3]=allowable_moves[3] && P.lattice.is_xz_plane_free(x,y+2,z);
    allowable_moves[4]=allowable_moves[4] && P.lattice.is_xy_plane_free(x,y,z-2);
    allowable_moves[5]=allowable_moves[5] && P.lattice.is_xy_plane_free(x,y,z+2);

    if constexpr (STIFF){
    move_rates[0]=allowable_moves[0] ? get_angle_rate(pos,{x-1,y,z},P):0.0;
    move_rates[1]=allowable_moves[1] ? get_angle_rate(pos,{x+1,y,z},P):0.0;
    move_rates[2]=allowable_moves[2] ? get_angle_rate(pos,{x,y-1,z},P):0.0;
    move_rates[3]=allowable_moves[3] ? get_angle_rate(pos,{x,y+1,z},P):0.0;
    move_rates[4]=allowable_moves[4] ? get_angle_rate(pos,{x,y,z-1},P):0.0;
    move_rates[5]=allowable_moves[5] ? get_angle_rate(pos,{x,y,z+1},P):0.0;
    }
    else{
    move_rates[0]=allowable_moves[0] ? 1.0:0.0;
    move_rates[1]=allowable_moves[1] ? 1.0:0.0;
    move_rates[2]=allowable_moves[2] ? 1.0:0.0;
    move_rates[3]=allowable_moves[3] ? 1.0:0.0;
    move_rates[4]=allowable_moves[4] ? 1.0:0.0;
    move_rates[5]=allowable_moves[5] ? 1.0:0.0;
    }

    return;
}


void Move::update_allowed_move(const Polymer &P,int dir2)
{
    //dir2 in the range [-6,..,-1,1,..6]
    const  std::array<int, 3> &pos=P.monomer_loc3[n];
    const int x=pos[0];
    const int y=pos[1];
    const int z=pos[2];
    bool is_bridged=P.bridges[n];
    assert(dir2!=0);
    if(dir2<0) //negative mean move in direction -dir-1 is not allowed
    {
        move_rates[abs(dir2)-1]=0.0;
        return;
    }
    else
    {
        int axis=(dir2-1)/2;
        assert(axis==0 || axis==1 || axis==2);
        int dir=2*((dir2-1)%2)-1;//-1 or +1, direction along axis
        assert(dir==+1 || dir==-1);

        switch(axis)
        {
        case 0:
        {
            allowable_moves[dir2-1]=is_bridged ? check_bridge_length({x+dir,y,z},P.monomer_loc3[P.bridges[n]-1]):1;
            size_t i=0;
            while(i<n_neigh)
            {
                const std::array<int, 3> &pos_neigh=P.monomer_loc3[P.monomer_neighbours[n][i]];
                allowable_moves[dir2-1]=allowable_moves[dir2-1] && check_length({x+dir,y,z},pos_neigh);//the new position is on the lattice because of is_XX_plane_free below checks that x-2 is in [0,Nx-1]
                i++;
            }
            allowable_moves[dir2-1]=allowable_moves[dir2-1] && P.lattice.is_yz_plane_free(x+2*dir,y,z);
            if constexpr (STIFF)
            {
                move_rates[dir2-1]=allowable_moves[dir2-1] ? get_angle_rate(pos, {x+dir,y,z},P):0.0;
            }
            else
            {
                move_rates[dir2-1]=allowable_moves[dir2-1] ? 1.0:0.0;
            }
            return;
        }

        case 1:
        {
            allowable_moves[dir2-1]=is_bridged ? check_bridge_length({x,y+dir,z},P.monomer_loc3[P.bridges[n]-1]):1;
            size_t i=0;
            while(i<n_neigh)
            {
                const std::array<int, 3> &pos_neigh=P.monomer_loc3[P.monomer_neighbours[n][i]];
                allowable_moves[dir2-1]=allowable_moves[dir2-1] && check_length({x,y+dir,z},pos_neigh);//the new position is on the lattice because of is_XX_plane_free below checks that x-2 is in [0,Nx-1]
                i++;
            }
            allowable_moves[dir2-1]=allowable_moves[dir2-1] && P.lattice.is_xz_plane_free(x,y+2*dir,z);
            if constexpr (STIFF)
            {
                move_rates[dir2-1]=allowable_moves[dir2-1] ? get_angle_rate(pos, {x,y+dir,z},P):0.0;
            }
            else
            {
                move_rates[dir2-1]=allowable_moves[dir2-1] ? 1.0:0.0;
            }
            return;

        }

        case 2:
        {
            allowable_moves[dir2-1]=is_bridged ? check_bridge_length({x,y,z+dir},P.monomer_loc3[P.bridges[n]-1]):1;
            size_t i=0;
            while(i<n_neigh)
            {
                const std::array<int, 3> &pos_neigh=P.monomer_loc3[P.monomer_neighbours[n][i]];
                allowable_moves[dir2-1]=allowable_moves[dir2-1] && check_length({x,y,z+dir},pos_neigh);//the new position is on the lattice because of is_XX_plane_free below checks that x-2 is in [0,Nx-1]
                i++;
            }
            allowable_moves[dir2-1]=allowable_moves[dir2-1] && P.lattice.is_xy_plane_free(x,y,z+2*dir);
            if constexpr (STIFF)
            {
                move_rates[dir2-1]=allowable_moves[dir2-1] ? get_angle_rate(pos, {x,y,z+dir},P):0.0;
            }
            else
            {
                move_rates[dir2-1]=allowable_moves[dir2-1] ? 1.0:0.0;
            }
            return;

        }

        }

    }
}





inline bool Move::check_length(const std::array<int,3> &v1,const std::array<int,3> &v2)
{
    int d=(v1[0]-v2[0])*(v1[0]-v2[0])+(v1[1]-v2[1])*(v1[1]-v2[1])+(v1[2]-v2[2])*(v1[2]-v2[2]);
    //std::cout << d << '\t';
    return (d<11) ? check_bond_length[d]:0;
}

//inline bool Move::check_length(const int v10,const int v11,const int v12,const int v20,const int v21,const int v22)
//{
//    int d=(v10-v20)*(v10-v20)+(v11-v21)*(v11-v21)+(v12-v22)*(v12-v22);
//    //std::cout << d << '\t';
//
//    assert(d<11);
//    return check_bond_length[d];
//}

//inline bool Move::check_length_d(const std::array<int,3> &v1)
//{
//    int d=v1[0]*v1[0]+v1[1]*v1[1]+v1[2]*v1[2];
//    //std::cout << d << '\t';
//    assert(d<11);
//    return check_bond_length[d];
//}

//inline bool Move::check_length_d(const int v0,const int v1,const int v2)
//{
//    int d=v0*v0+v1*v1+v2*v2;
//    //std::cout << d << '\t';
//    assert(d<11);
//    return check_bond_length[d];
//}

inline bool Move::check_bridge_length(const std::array<int,3> &v1,const std::array<int,3> &v2)
{
    int d=(v1[0]-v2[0])*(v1[0]-v2[0])+(v1[1]-v2[1])*(v1[1]-v2[1])+(v1[2]-v2[2])*(v1[2]-v2[2]);
    return (d<9) ? 1:0;
}


inline double Move::get_angle_rate(const std::array<int,3> &v0, const std::array<int,3> &v0_n, const Polymer &P){

    float deltaE=0.0;
    //double k=7.0;

    if (n_neigh==2)
    {
        //where v_*l is to the left of the chosen monomer and v_*r to the right and _n represents new vector
        std::array<int,3> v1l(P.monomer_loc3[P.monomer_neighbours[n][0]]);
        std::array<int,3> v1r(P.monomer_loc3[P.monomer_neighbours[n][1]]);

        //int n_neighL=P.monomer_neighbours[n-1].size();
        //int n_neighR=P.monomer_neighbours[n+1].size();

        if (n_neighL==1)
        {
            std::array<int,3> v2r(P.monomer_loc3[P.monomer_neighbours[n+1][1]]);
            std::array<int,3> v1l_n(v0_n-v1l);
            std::array<int,3> v1r_n(v1r-v0_n);

            v2r-=v1r;
            v1r-=v0;
            v1l-=v0;//minus 1 times what we want

            float x1 = get_cos_angle(v1l,v1r);
            float x2 = -get_cos_angle(v1r,v2r);
            float x1_n = -get_cos_angle(v1l_n,v1r_n);
            float x2_n = -get_cos_angle(v1r_n,v2r);

            deltaE = (x1_n + x2_n - x1 - x2);
            //deltaE = (x1_n*x1_n + x2_n*x2_n - x1*x1 - x2*x2);
        }
        else if (n_neighR==1)
        {
            std::array<int,3> v2l(P.monomer_loc3[P.monomer_neighbours[n-1][0]]);
            std::array<int,3> v1l_n(v0_n-v1l);
            std::array<int,3> v1r_n(v1r-v0_n);

            v2l-=v1l;//minus 1 times what we want
            v1l-=v0;//minus 1 times what we want
            v1r-=v0;

            float x1 = -get_cos_angle(v2l,v1l);
            float x2 = get_cos_angle(v1l,v1r);
            float x1_n = get_cos_angle(v2l,v1l_n);
            float x2_n = -get_cos_angle(v1l_n,v1r_n);

            deltaE = (x1_n + x2_n -x1 -x2);
            //deltaE =  (x1_n*x1_n + x2_n*x2_n - x1*x1 - x2*x2);
        }
        else
        {


            std::array<int,3> v2l(P.monomer_loc3[P.monomer_neighbours[n-1][0]]);
            std::array<int,3> v2r(P.monomer_loc3[P.monomer_neighbours[n+1][1]]);

            std::array<int,3> v1l_n(v0_n-v1l);
            std::array<int,3> v1r_n(v1r-v0_n);

            v2l-=v1l;//minus 1 times what we want
            v1l-=v0;//minus 1 times what we want
            v2r-=v1r;
            v1r-=v0;
//
//            float v1l_s=InvSqrtImpl_3(float(v1l*v1l));// 1/length
//            float v2l_s=InvSqrtImpl_3(float(v2l*v2l));
//            float v1r_s=InvSqrtImpl_3(float(v1r*v1r));
//            float v2r_s=InvSqrtImpl_3(float(v2r*v2r));
//
//            float v1l_n_s=InvSqrtImpl_3(float(v1l_n*v1l_n));
//            float v1r_n_s=InvSqrtImpl_3(float(v1r_n*v1r_n));
//
//            float x1 = -(v2l*v1l)*v2l_s*v1l_s;//get_cos_angle(v2l,v1l);
//            float x2 = (v1l*v1r)*v1l_s*v1r_s;//get_cos_angle(v1l,v1r);
//            float x3 = -(v1r*v2r)*v1r_s*v2r_s;//get_cos_angle(v1r,v2r);
//            float x1_n = (v2l*v1l_n)*v2l_s*v1l_n_s;//get_cos_angle(v2l,v1l_n);
//            float x2_n = -(v1l_n*v1r_n)*v1l_n_s*v1r_n_s;//get_cos_angle(v1l_n,v1r_n);
//            float x3_n = -(v1r_n*v2r)*v1r_n_s*v2r_s;//get_cos_angle(v1r_n,v2r);

            float x1 = -get_cos_angle(v2l,v1l);
            float x2 = get_cos_angle(v1l,v1r);
            float x3 = -get_cos_angle(v1r,v2r);
            float x1_n = get_cos_angle(v2l,v1l_n);
            float x2_n = -get_cos_angle(v1l_n,v1r_n);
            float x3_n = -get_cos_angle(v1r_n,v2r);

            deltaE = (x1_n + x2_n + x3_n - x1 - x2 - x3);
            //deltaE = (x1_n*x1_n + x2_n*x2_n + x3_n*x3_n - x1*x1 - x2*x2 - x3*x3);
        }
    }

    else if (n_neigh==1)
    {
        float x=0.0f;
        float x_n=0.0f;

        if (n==0)
        {
            std::array<int,3> v1r(P.monomer_loc3[P.monomer_neighbours[n][0]]);//find location of neighbouring monomer
            std::array<int,3> v2r(P.monomer_loc3[P.monomer_neighbours[n+1][1]]);//neighbours are listed as (i-1,i+1)
            std::array<int,3> v1r_n(v1r-v0_n);

            v2r-=v1r;
            v1r-=v0;

            x = -get_cos_angle(v1r,v2r);
            x_n = -get_cos_angle(v1r_n,v2r);
        }
        else
        {
            std::array<int,3> v1l(P.monomer_loc3[P.monomer_neighbours[n][0]]);//find location of neighbouring monomer
            std::array<int,3> v2l(P.monomer_loc3[P.monomer_neighbours[n-1][0]]);
            std::array<int,3> v1l_n(v0_n-v1l);

            v2l-=v1l;//minus 1 times what we want
            v1l-=v0;//minus 1 times what we want

            x = -get_cos_angle(v1l,v2l);
            x_n = +get_cos_angle(v1l_n,v2l);
        }


        deltaE = (x_n - x);
        //deltaE = (x_n*x_n - x*x);
    }
    else
    {
        std::cout << "More than 2 neighbours!";
    }

   // std::cout << deltaE << '\n';
    return deltaE<0.0f ? 1.0 : exp(-kp*deltaE);//fast_exp is faster but is sometimes negative for small arguements
}

inline float Move::get_cos_angle(const std::array<int,3> &b1, const std::array<int,3> &b2)
{
    int dotprod=0;//b1*b2;
    int t1=0;//b1*b1;
    int t2=0;//b2*b2;

    for (int i=0; i<3; ++i)
    {
        dotprod+=b1[i]*b2[i];
        t1+=b1[i]*b1[i];
        t2+=b2[i]*b2[i];
    }


    float out= ( dotprod *InvSqrtImpl_3(float(t1)) *InvSqrtImpl_3(float(t2)));//automatic cast to double inside sqrt since c++11, even though t1*t2 is ok
    //since int is 32 bit on a 64bit machine, keeping them searate gives more consistent timings

    //return ( dotprod *sqrt2[t1]*sqrt2[t2]);
    //if(out<0.1) return -10000.0; else
    return out;
}

inline float Move::get_cos_angle(int &b11, int &b12, int &b13, int &b21, int &b22, int &b23)
{
    int dotprod=b11*b21 + b12*b22 + b13*b23;
    int t1=b11*b11 + b12*b12 + b13*b13;
    int t2=b21*b21 + b22*b22 + b23*b23;

    return ( dotprod *InvSqrtImpl_3(float(t1)) *InvSqrtImpl_3(float(t2)));
}




void Move::update_non_neighbour_monomers_affected(const int x,const int y,const int z,const int axis, const int dir, const Polymer &P)
{
    //x,y,z is the old position
    reactionsAffected.resize(neighbourhoodsize);

    non_neighbour_monomers_affected.resize(0);
    int m;

    auto dir2=[](int axis,int dir){return 2*axis+(dir+1)/2+1;}; //transform to range 1 to 6


    switch(axis)
    {
    case 0:{
        for(int j=-1; j<=1; ++j) //42 checks, however if one is positive the immediate neighbours do not need to be checked since they cannot contain a monomer
        {
            //monomers that could move in the direction parallel to the motion

            for(int k=-1; k<=1; ++k)
            {
                m=P.lattice.get_monomer(x-2*dir,y+j,z+k);//monomer here may now be allowed to move in the dir*x direction
                if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(axis,dir));

                m=P.lattice.get_monomer(x+3*dir,y+j,z+k);//monomer here are not allowed to move in the -dir*x direction
                if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(axis,-dir));
            }

            //monomers that could move in the direction perpendicular to the motion
            m=P.lattice.get_monomer(x-dir,y+2,z+j);//monomer here may now be allowed to move in the -y direction
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(1,-1));
            m=P.lattice.get_monomer(x-dir,y-2,z+j);//monomer here may now be allowed to move in the +y direction
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(1,1));

            m=P.lattice.get_monomer(x-dir,y+j,z+2);//monomer here may now be allowed to move in the -z direction
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(2,-1));
            m=P.lattice.get_monomer(x-dir,y+j,z-2);//monomer here may now be allowed to move in the +z direction
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(2,1));

            m=P.lattice.get_monomer(x+2*dir,y+2,z+j);//monomer here are not allowed to move in the -y direction
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(1,-1));
            m=P.lattice.get_monomer(x+2*dir,y-2,z+j);//monomer here are not allowed to move in the +y direction
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(1,1));

            m=P.lattice.get_monomer(x+2*dir,y+j,z+2);//monomer here are not allowed to move in the -z direction
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(2,-1));
            m=P.lattice.get_monomer(x+2*dir,y+j,z-2);//monomer here are not allowed to move in the +z direction
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(2,1));
        }
        break;
    }

    case 1:{
        for(int j=-1; j<=1; ++j) //42 checks, however if one is positive the immediate neighbours do not need to be checked since they cannot contain a monomer
        {
            //monomers that could move in the direction parallel to the motion
            for(int k=-1; k<=1; ++k)
            {
                m=P.lattice.get_monomer(x+j,y-2*dir,z+k);
                if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(axis,dir));

                m=P.lattice.get_monomer(x+j,y+3*dir,z+k);
                if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(axis,-dir));
            }

            //monomers that could move in the direction perpendicular to the motion
            m=P.lattice.get_monomer(x+2,y-dir,z+j);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(0,-1));
            m=P.lattice.get_monomer(x-2,y-dir,z+j);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(0,1));

            m=P.lattice.get_monomer(x+j,y-dir,z+2);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(2,-1));
            m=P.lattice.get_monomer(x+j,y-dir,z-2);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(2,1));

            m=P.lattice.get_monomer(x+2,y+2*dir,z+j);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(0,-1));
            m=P.lattice.get_monomer(x-2,y+2*dir,z+j);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(0,1));

            m=P.lattice.get_monomer(x+j,y+2*dir,z+2);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(2,-1));
            m=P.lattice.get_monomer(x+j,y+2*dir,z-2);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(2,1));
        }
        break;
    }

    case 2:{
        for(int j=-1; j<=1; ++j) //42 checks, however if one is positive the immediate neighbours do not need to be checked since they cannot contain a monomer
        {
            //monomers that could move in the direction parallel to the motion
            for(int k=-1; k<=1;++k)
            {
                m=P.lattice.get_monomer(x+j,y+k,z-2*dir);
                if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(axis,dir));

                m=P.lattice.get_monomer(x+j,y+k,z+3*dir);
                if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(axis,-dir));
            }

            //monomers that could move in the direction perpendicular to the motion
            m=P.lattice.get_monomer(x+2,y+j,z-dir);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(0,-1));
            m=P.lattice.get_monomer(x-2,y+j,z-dir);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(0,1));

            m=P.lattice.get_monomer(x+j,y+2,z-dir);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(1,-1));
            m=P.lattice.get_monomer(x+j,y-2,z-dir);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,dir2(1,1));

            m=P.lattice.get_monomer(x+2,y+j,z+2*dir);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(0,-1));
            m=P.lattice.get_monomer(x-2,y+j,z+2*dir);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(0,1));

            m=P.lattice.get_monomer(x+j,y+2,z+2*dir);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(1,-1));
            m=P.lattice.get_monomer(x+j,y-2,z+2*dir);
            if(m>0) non_neighbour_monomers_affected.emplace_back(m-1,-dir2(1,1));
        }
        break;
    }
    default:
        std::cout<<"error in move:update_non_neighbour_monomers_affected" << '\n';
    }


//    std::cout << "monomers affected:";
//    for(auto const &x: non_neighbour_monomers_affected)
//        {
//            std::cout << '\t'<< x.first ;
//        }
//
//   std::cout << "\n";
    auto is_neighbour=[this](size_t n){return std::any_of(reactionsAffected.begin(),reactionsAffected.end(),[n](size_t n2){return n2==n;});};

    auto removed = std::remove_if(non_neighbour_monomers_affected.begin(),non_neighbour_monomers_affected.end(),
        [this,is_neighbour](std::pair<size_t,int> x) {return is_neighbour(x.first);}
        );
    non_neighbour_monomers_affected.erase(removed, non_neighbour_monomers_affected.end());

    if(P.bridges[n]){
    reactionsAffected.emplace_back(P.bridges[n]-1);
    }

//   std::cout << "monomers affected:";
//    for(auto const &x: non_neighbour_monomers_affected)
//        {
//            std::cout << '\t'<< x.first ;
//        }
//
//   std::cout << "\n";
}
