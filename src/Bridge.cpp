#include "Bridge.h"

unsigned long long int Bridge::totalcount=0;


double Bridge::rate(const Polymer& P)
{
    //std::cout << "rate0" << '\t';
//    find_allowable_bridges(P);
//    int rate=0;
//    for(int i=0; i<6; i++)
//    {
//        //std::cout << allowable_moves[i] << '\t';
//        rate+=(int)allowable_bridges[i];
//    }
//    //std::cout <<"rate1" <<'\n';
//    return rate*rate_const;
    return rate_const;
}

void Bridge::do_reaction(Polymer& P, double rx_u, double rate)
{
    assert(rate=this->rate(P));

    if(P.bridges[n])
    {
        //std::cout << "already bridged" << '\n';
        reactionsAffected.resize(0);
        return;// if this monomer is already bridged then do nothing
    }

    //pick the axis and direction to check
    int m=6*rx_u/rate_const;//rounds down by default
    assert(m<6);

    int axis=m/2;

    int dir=2*(m%2)-1;
    assert(dir==-1 || dir==1);

    double s=6*rx_u/rate_const-m;
    int ax1=3*s;//which position on one axis of the 3x3 plane

    double t=3.0*s-ax1;
    int ax2=3*t;//which position on other axis of the 3x3 plane

    //ax1=1;//uncomment to restrict bridge formation to length 2
    //ax2=1;

    std::array<int, 3> &ind=P.monomer_loc3[n];
    int x=ind[0];
    int y=ind[1];
    int z=ind[2];


    unsigned int n2=0;

    assert(axis<3);

    //std::cout << x << '\t' << y <<'\t' << z << '\t' << ax1  <<'\t' << ax2 << '\t' << dir <<'\n';
    switch(axis)
    {
    case 0:
    {
        n2=P.lattice.get_monomer(x+2*dir,y+ax1-1,z+ax2-1);
        break;
    }
    case 1:
    {
        n2=P.lattice.get_monomer(x+ax1-1,y+2*dir,z+ax2-1);
        break;
    }
    case 2:
    {
        n2=P.lattice.get_monomer(x+ax1-1,y+ax2-1,z+2*dir);
        break;
    }
    default:
        std::cout<<"error in bridge:do_reaction" << '\n';
    }

    //std::cout << n2 <<'\n';

    //if the selected monomer is not a neighbour and is not already bridged
    if(n2>0 && std::none_of(P.monomer_neighbours[n].begin(), P.monomer_neighbours[n].end(), [&n2](const unsigned int &N1)->bool{return N1==(n2-1);}) && !P.bridges[n2-1])
    {
        P.bridges[n]=n2;//the nth monomer is bridged to the n2-1 th monomer
        P.bridges[n2-1]=n+1;//the n2-1th monomer is bridged to the n th monomer

        P.n_bridged_monomers=P.n_bridged_monomers+2;

        //auto pos_new=P.monomer_loc3[n2-1];
        //std::cout << "made a bridge between  monomer " << n <<" (" <<x<<','<<y<<','<<z<<") and monomer "<< n2-1 <<" (" <<pos_new[0]<<','<<pos_new[1]<<','<<pos_new[2]<<')' <<'\n';

        //update_reactionsAffected(x,y,z,axis,dir,P);
        reactionsAffected.resize(3);
        reactionsAffected[0]=n;
        reactionsAffected[1]=n2-1;
        reactionsAffected[2]=P.monomer_loc.size();//unbridging

        totalcount++;
        return;
    }
    else
    {
        reactionsAffected.resize(0);
        return;
    }
}


