# include <iostream>	// cout, etc.
#include <vector>
#include "Polymer.h"
#include "Move.h"
#include "Bridge.h"
#include "Unbridge.h"
#include "GillespieSimulation.h"


using namespace std ;


int main(int argc,char *argv[]){

Lattice lattice;
const unsigned int N=430;//10*(Nx-1)/2;//polymer length

std::vector<lattice_index_type> loc;
//loc.resize(N);

if(argc==2){
   std::string input=argv[1];

    ifstream in;
    in.open(input);
    if (!in) {
        cout << "Unable to open file";
        exit(1); // terminate with error
    }
    int ind;
    while (in >> ind) {
        loc.emplace_back(ind);
    }
    if(loc.size()!=N){
        cout << "input polymer does not have the correct length";
        exit(1);
    }

}
else{
    cout << "using default initial polymer";
//create an initial polymer by overlaying it longitudinally, while respecting bond length conditions
    loc.resize(N);
int y0=30;//staring y position, 0 for Ny=22
int z=45;//z position, any value with acceptable range


for(unsigned int i=0;i<(Nx-1)/2;i++){
    loc[i]=lattice.get_single_index(2*i+1,y0+1,z);
}


for(unsigned int i=0;i<(Nx-3)/2;i++){
    loc[(Nx-1)/2+i]=lattice.get_single_index(Nx-2*i-2,y0+3,z);

    loc[(Nx-1)/2+(Nx-3)/2+i]=lattice.get_single_index(2*i+3,y0+5,z);
    loc[(Nx-1)/2+2*(Nx-3)/2+i]=lattice.get_single_index(Nx-2*i-2,y0+7,z);
    loc[(Nx-1)/2+3*(Nx-3)/2+i]=lattice.get_single_index(2*i+3,y0+9,z);
    loc[(Nx-1)/2+4*(Nx-3)/2+i]=lattice.get_single_index(Nx-2*i-2,y0+11,z);
    loc[(Nx-1)/2+5*(Nx-3)/2+i]=lattice.get_single_index(2*i+3,y0+13,z);
    loc[(Nx-1)/2+6*(Nx-3)/2+i]=lattice.get_single_index(Nx-2*i-2,y0+15,z);
    loc[(Nx-1)/2+7*(Nx-3)/2+i]=lattice.get_single_index(2*i+3,y0+17,z);
    loc[(Nx-1)/2+8*(Nx-3)/2+i]=lattice.get_single_index(Nx-2*i-2,y0+19,z);
}
    loc[(Nx-1)/2+9*(Nx-3)/2+0]=lattice.get_single_index(1,y0+19,z);

    for(int i=0;i<8;i++){
        loc[(Nx-1)/2+9*(Nx-3)/2+1+i]=lattice.get_single_index(1,y0+17-2*i,z);
}
    //starts at (1,y0+1,z), finishes at (1,y0+3,z)


}
std::vector<std::vector<unsigned int>> neighbours;
neighbours.resize(N);
neighbours[0]={1};
for(unsigned int i=1;i<N-1;i++){
    neighbours[i]={i-1,i+1};
}
neighbours[N-1]={N-2};

Polymer P(lattice,loc,neighbours);//lattice stored as reference
std::cout << "Polymer length: "<<loc.size() <<'\n' << "Initial: " <<'\n';
P.print();
std::cout << '\n';

ofstream out;
out.open("output.txt");
ofstream out2;
out2.open("outputbridges.txt");
out << Nx << '\t' << Ny << '\t' << Nz << '\n';
//P.print(out,0.0);

ReactionsArray RX;
RX.resize(2*N+1);
double p=400.0;
double k_un=1.0;
double k=200*k_un;
for(unsigned int i=0; i<N; i++){
        RX[i].reset(new Move(p,7.0,i,neighbours));//~17 for CSA
        RX[N+i+1].reset(new Bridge(k,i));
        // need to use reset since we cannot use an initialization list - shared pointers are initialized to null when declared without an initialization
}
RX[N].reset(new Unbridge(k_un));

GillespieSimulation gs(RX,P);//P stored as reference
double timestep=1.0;
double finaltime=1000.0;
double t=0.0;

//gs.advance_time(2/k_un);//wait  lifetimes //~hundreds of lifetimes needed for the SFF to stabilise
P.print(out,0.0);
P.print_bridges(out2);


//time-based:
while(t<finaltime){
    gs.advance_time(timestep);
    t+=timestep;
    P.print(out,t);
    P.print_bridges(out2);
}


//step-based
//int n_steps=100000000;
//int step=0;
//
//while(step<n_steps){
//    //gs.advance_time(timestep);
//    gs.advance_step(1000000);
//    step+=1000000;
//    P.print(out,step);
//    P.print_bridges(out2);
//}


std::cout << "Final: " <<'\n';
P.print();


std::cout << "Number of moves: " << Move::totalcount << '\n';
std::cout << "Number of bridges made: " << Bridge::totalcount << '\n';
std::cout << "Number of bridges broken: " << Unbridge::totalcount << '\n';
std::cout << "Final number of bridges: " << P.n_bridged_monomers;
return 0;


}
