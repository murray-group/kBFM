#ifndef MOVE_H
#define MOVE_H
//class for moving a specific monomer
#include <cmath>
# include <array>
#include "Reaction.h"
#include "Polymer.h"

//#include <emmintrin.h>
//#ifdef __SSE4_1__
//#include <smmintrin.h>
//#endif

const bool check_bond_length[11]={0,0,0,0,1,1,1,0,0,1,1};

//constexpr double isqrt(int n){return 1.0/sqrt(n);}
//inline const double sqrt2[11]={isqrt(0),isqrt(1),isqrt(2),isqrt(3),isqrt(4),isqrt(5),isqrt(6),isqrt(7),isqrt(8),isqrt(9),isqrt(10)};

class Move : public Reaction
{
public:
    static unsigned long long int totalcount;



    Move(double rate_in, double kp, unsigned int n,std::vector<std::vector<unsigned int>> &monomer_neighbours):kp(kp),n(n),
    n_neigh(monomer_neighbours[n].size()),
    n_neighL(n_neigh==2 ? monomer_neighbours[monomer_neighbours[n][0]].size():0),
    n_neighR(n_neigh==2 ? monomer_neighbours[monomer_neighbours[n][1]].size():0),allowable_moves{0,0,0,0,0,0}

    {
        rate_const=rate_in;
        reactionsAffected.resize(0);
        reactionsAffected.emplace_back(n);//this monomer
        reactionsAffected.insert(reactionsAffected.end(),monomer_neighbours[n].begin(),monomer_neighbours[n].end());//the two neighours since the bond lengths change
        neighbourhoodsize=reactionsAffected.size();

        if constexpr (STIFF){
            for(auto n2 : monomer_neighbours[n])
            reactionsAffected.insert(reactionsAffected.end(),monomer_neighbours[n2].begin(),monomer_neighbours[n2].end());//the neighbours of the neighbours when simulating a stiff polymer

            sort( reactionsAffected.begin(), reactionsAffected.end() );
            reactionsAffected.erase( unique( reactionsAffected.begin(), reactionsAffected.end() ), reactionsAffected.end() );
            neighbourhoodsize=reactionsAffected.size();
        }
    };

    virtual ~Move();

    void do_reaction(Polymer& P, double rx_u, double rate);

    double rate(const Polymer& P);
    double rate(const Polymer& P,int dir2);



protected:

private:
    const float kp;//parameter for stiffness potential
    const unsigned int n;
    const unsigned int n_neigh;
    const unsigned int n_neighL;
    const unsigned int n_neighR;
    unsigned int neighbourhoodsize;

    std::array<bool,6> allowable_moves;
    std::array<double,6> move_rates;





    void find_allowable_moves(const Polymer &P);
    void update_allowed_move(const Polymer &P,int dir);

    //static means one copy of this function can be shared across all instances. This is ok because these functions do not depends on any member variables e.g. n
    inline static bool check_bridge_length(const std::array<int,3> &v1,const std::array<int,3> &v2);
    inline static bool check_length(const std::array<int,3> &v1,const std::array<int,3> &v2);
    //inline static bool check_length(const int v10,const int v11,const int v12,const int v20,const int v21,const int v22);
   // inline static bool check_length_d(const std::array<int,3> &v1);
    //inline static bool check_length_d(const int v0,const int v1,const int v2);

    inline static float get_cos_angle(int &w11, int &w12, int &w13, int &w21, int &w22, int &w23);
    inline static float get_cos_angle(const std::array<int,3> &w1, const std::array<int,3> &w2);
    inline double get_angle_rate(const std::array<int,3> &v0, const std::array<int,3> &v0_n, const Polymer &P);//inline bool for MC


    void update_non_neighbour_monomers_affected(const int x,const int y,const int z,const int axis, const int dir, const Polymer &P);


    /** Let's use SSE2 instruction and leverage on hardware
*  https://gcc.gnu.org/onlinedocs/gcc/Extended-Asm.html
*/
inline static float InvSqrtImpl_3(float x)
{
    float y;

    asm
    (
            "rsqrtss %[x], %%xmm0;"   // EVAL rsqrtss of "x" and store result in xmm0
            "movss %%xmm0, %[y];"     // LOAD value from xmm0 into y
        :
        : [ x ] "m" ( x ),
          [ y ] "m" ( y )
          /*[ [asmSymbolicName] ] constraint (cvariablename)
                                     'm' -- memory operand
          */
        : "xmm0"
    );

    return y;
}

 /* max. rel. error <= 1.73e-3 on [-87,88] */
inline static float fast_exp (float x)
 {
     //if(x<-1000) return 0.0;
   volatile union {
     float f;
     unsigned int i;
   } cvt;

   /* exp(x) = 2^i * 2^f; i = floor (log2(e) * x), 0 <= f <= 1 */
   float t = x * 1.442695041f;
   float fi = floorf (t);
   float f = t - fi;
   int i = (int)fi;
   cvt.f = (0.3371894346f * f + 0.657636276f) * f + 1.00172476f; /* compute 2^f */
   cvt.i += (i << 23);                                          /* scale by 2^i */
   return cvt.f;
 }


public:
    std::vector<std::pair<size_t,int>> non_neighbour_monomers_affected;

};



#endif // MOVE_H
