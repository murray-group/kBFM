#ifndef REACTION_H
#define REACTION_H


//__________________________________________
# include <vector>	// for std::vector<>
# include <array>
# include "definitions.h"  //has container definition
# include "Polymer.h"
//______________________________________________

# include <iostream>	// cout, etc.


//
class Reaction
{
    // All reactions have these
public:
    double	rate_const ;

    std::vector<size_t>	reactionsAffected ;//additional reactions to be updated (specified by the user)

    Reaction():rate_const(0.0),reactionsAffected{} {};
    virtual void do_reaction(Polymer &P, double rx_u, double rate)=0; //virtual means that derived class version of the function will be used where it exists, even if a pointer to Reaction is used


    // The member defined below is the one that makes
    // this a virtual base class: given the volume and
    // the populations of all the chemical species,
    // derived classes must know how to compute their own rates.
    virtual ~Reaction() {};

    virtual void increasecounter() const {}; //const member functions can still modify static data members;
    //It has no definition in Reaction. Children must define it.
    //Makes Reaction an abstract class. maybe unnecceasary. a virtual function should be sufficient and then we could use a
    //vector of Reactions and not a vector of Reaction*. Then we would not have to use new/shared_ptr.
    //***wrong** polymorphism will not work. a Binding instance will be converted to a Reaction instance and any additional members
    //in Binding that are not in Reaction will be sliced off (object slicing).
    virtual double rate(const Polymer &P) =0;//for some weird cases like GlobalBinding, cannot be a const member function

} ;

#endif // REACTION_H
