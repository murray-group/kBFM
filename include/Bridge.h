#ifndef BRIDGE_H
#define BRIDGE_H

#include <Reaction.h>
#include <algorithm> //none_of
#include <math.h>

class Bridge : public Reaction
{
    public:
    static unsigned long long int totalcount;


    Bridge(double rate_in,unsigned int n):n(n)
    {
        rate_const=rate_in;
    };


    void do_reaction(Polymer& P, double rx_u, double rate);

    double rate(const Polymer& P);



protected:

private:

    const unsigned int n;

};

#endif // BRIDGE_H
