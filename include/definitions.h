#ifndef definitions_H_INCLUDED
#define definitions_H_INCLUDED
# include <vector>		// std::vector<>
# include <array>
# include <memory>

#include <algorithm>
#include <functional>

//# define NDEBUG //comment out to turn on assert. costs ~10%
# include <assert.h>	// for assert()

const bool STIFF=1;

//_______________________________________________
//declare global constants, allows compiler optimisation of division and modulus
const unsigned int Nx=150;//index scheme counts in the y-z plane first
const unsigned int Ny=150;
const unsigned int Nz=150;
const unsigned int Nyz=Ny*Nz;


class Reaction; //forward declaration
typedef class std::shared_ptr<Reaction>	ReactionPtr ; // a handy nickname
typedef std::vector<ReactionPtr>	ReactionsArray ; // a handy nickname, vector cannot contain Reaction, only Reaction* since Reaction is an abstract class



template <typename T>
inline std::vector<T> concat(const std::vector<T> &A, const std::vector<T> &B)//concatenate vectors, returns new vector
{
    std::vector<T> AB;
    AB.reserve( A.size() + B.size() );                // preallocate memory
    AB.insert( AB.end(), A.begin(), A.end() );        // add A;
    AB.insert( AB.end(), B.begin(), B.end() );        // add B;
    return AB;
}

template <typename T>
inline std::vector<T> & concat_in_place(std::vector<T> &A, const std::vector<T> &B)//concatenate vectors, appends the second to the first
{
    //A.reserve( A.size() + B.size() );                // preallocate memory
    A.insert( A.end(), B.begin(), B.end() );        // add B;
    return A;
}




template <typename T>
inline std::vector<T> operator+(const std::vector<T> &A, const std::vector<T> &B)//add vectors, returns new vector
{
    assert(A.size()==B.size());

    std::vector<T> result;
    result.reserve( A.size());                // preallocate memory

    std::transform(A.begin(), A.end(), B.begin(), std::back_inserter(result), std::plus<T>());
    return result;
}

template <typename T>
inline std::vector<T> & operator+=(std::vector<T> &A, const std::vector<T> &B)//+= add vectors, returns by reference
{
    assert(A.size()==B.size());

    std::transform(A.begin(), A.end(), B.begin(), A.begin(), std::plus<T>());
    return A;
}


template <typename T>
inline std::vector<T> operator+(const std::vector<T> &A, T &B)//add scalar, returns new vector
{
    std::vector<T> AB;
    AB.reserve( A.size());                // preallocate memory
    AB=A;
    for(T& d : AB)
    {
        d += B;
    }
    return AB;
}

template <typename T>
inline std::vector<T> & operator+=(std::vector<T> &A, T &B)//+= add scalar, returns by reference
{
    for(T& d : A)
    {
        d += B;
    }
    return A;
}

template <typename T, size_t N>
inline std::array<T,N> operator-(const std::array<T,N> &A, const std::array<T,N> &B)//subtracts arrays, returns new array
{
    assert(A.size()==B.size());

    std::array<T,N> result;

    std::transform(A.begin(), A.end(), B.begin(), result.begin(), std::minus<T>());
    //for (int i=0; i<N; ++i){
    //    result[i]=A[i]-B[i];
    //}

    return result;
}

template <typename T, size_t N>
inline std::array<T,N> & operator-=(std::array<T,N> &A, const std::array<T,N> &B)//subtracts arrays, returns by reference
{
    assert(A.size()==B.size());

    std::transform(A.begin(), A.end(), B.begin(), A.begin(), std::minus<T>());
    //for (int i=0; i<N; ++i){
    //    A[i]=A[i]-B[i];
    //}

    return A;
}


template <typename T, size_t N>
inline T operator*(const std::array<T,N> &A, const std::array<T,N> &B)//dot product
{
    assert(A.size()==B.size());

    T out=0;
    for (int i=0; i<N; ++i){
        out += A[i]*B[i];
    }

    return out;
}



//enum { A = 0, B, C, D, E} ;

// See http://physics.nist.gov/cgi-bin/cuu/Value?na
const double	Avogadro_const = 6.02214179e23 ;



//_____





#endif // definitions_H_INCLUDED
