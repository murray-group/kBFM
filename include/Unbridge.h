#ifndef UNBRIDGE_H
#define UNBRIDGE_H

#include <Reaction.h>


class Unbridge : public Reaction
{
    public:
    static unsigned long long int totalcount;


    Unbridge(double rate_in)
    {
        rate_const=rate_in;
    };


    void do_reaction(Polymer& P, double rx_u, double rate);

    double rate(const Polymer& P);
    protected:

    private:
};

#endif // UNBRIDGE_H
