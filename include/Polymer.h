#ifndef POLYMER_H
#define POLYMER_H

# include <vector>
# include <array>
//# include <boost/numeric/ublas/vector_sparse.hpp>
//# include "boost/numeric/ublas/io.hpp"
# include <iostream>
# include <fstream>
#include <sstream>      // std::stringstream
# include "definitions.h"  //has container definition

typedef uint32_t lattice_index_type;//index used to label lattice sites, needs to be big enough given the lattice dimensions, could be size_t since it indices std::vector

class Lattice
{

private:
    //boost::numeric::ublas::compressed_vector<lattice_index_type> monomer_at;//monomer at each lattice site, returns monomer #+1, 0 if site is unoccupied
    std::vector<unsigned int> monomer_at;
public:
    //const unsigned int Nx, Ny, Nz, Nyz;//lattice dimensions


//    Lattice(unsigned int Nx,unsigned int Ny, unsigned int Nz):monomer_at{},Nx(Nx),Ny(Ny),Nz(Nz),Nyz(Ny*Nz)
//    {
//        monomer_at.resize(Nx*Ny*Nz,0);
//    }
    Lattice():monomer_at{}
    {
        monomer_at.resize(Nx*Ny*Nz,0);
    }

    //inline means that the compiler will try to remove the functional call by copying this code as required
    inline unsigned int get_monomer(int x, int y, int z) const
    {
        //assert(is_in_lattice(x,y,z));
        if(is_in_lattice(x,y,z)==true){
        return monomer_at[get_single_index(x,y,z)];}
        else return 0;
    }

    inline unsigned int get_monomer_without_checking(int x, int y, int z) const
    {
        assert(is_in_lattice(x,y,z));
        return monomer_at[get_single_index(x,y,z)];
    }

    inline unsigned int get_monomer(lattice_index_type ind) const
    {
        return monomer_at[ind];
    }

    inline void set_monomer(int x, int y, int z,unsigned int n)//monomer n-1 is at (x,y,z)
    {
        assert(is_in_lattice(x,y,z));
        monomer_at[get_single_index(x,y,z)]=n;
    }

    inline void set_monomer(lattice_index_type ind,unsigned int n)
    {
        monomer_at[ind]=n;
    }

    inline std::array<int,3> get_indices(lattice_index_type ind) const //convert from single index to 3 index notation
    {
        assert(ind>=0);
        assert(ind<Nx*Nyz);
        std::array<int,3> v{int(ind/(Nyz)),int((ind%(Nyz))/Nz), int((ind%(Nyz))%Nz)};//floor is automatic in integer division
        //assert(is_in_lattice(ind/(Ny*Nz),(ind%(Ny*Nz))/Nz, (ind%(Ny*Nz))%Nz));
        return v;
    }

    inline lattice_index_type get_single_index(int x, int y, int z) const //convert from 3 index notation to single index
    {
        assert(is_in_lattice(x,y,z));
        //assert(x*Nyz+y*Ny+z<Nx*Nyz);
        return lattice_index_type(x)*Nyz+lattice_index_type(y)*Nz+z;
    }

    inline bool is_in_lattice(int x, int y, int z) const //int in case x is negative
    {
        return ( (unsigned int)x<Nx && (unsigned int)y<Ny && (unsigned int)z<Nz);//negative values are looped around to big numbers by the cast to unsigned. In this way, only one inequality is required.
    }

    inline bool is_free(int x, int y, int z) const //is the site free
    {
        return (get_monomer_without_checking(x,y,z)==0);
    }

    inline bool is_yz_plane_free(int x, int y, int z) const  //are the site and its 8 neighbours in the plane free
    {
        return (unsigned)x< Nx && is_free(x,y-1,z-1) && is_free(x,y-1,z) && is_free(x,y-1,z+1) && is_free(x,y,z-1) && is_free(x,y,z) && is_free(x,y,z+1) && is_free(x,y+1,z-1) && is_free(x,y+1,z) && is_free(x,y+1,z+1);
    }

    inline bool is_xz_plane_free(int x, int y, int z) const  //are the site and its 8 neighbours in the plane free
    {
        return (unsigned)y< Ny && is_free(x-1,y,z-1) && is_free(x-1,y,z) && is_free(x-1,y,z+1) && is_free(x,y,z-1) && is_free(x,y,z) && is_free(x,y,z+1) && is_free(x+1,y,z-1) && is_free(x+1,y,z) && is_free(x+1,y,z+1);
    }

    inline bool is_xy_plane_free(int x, int y, int z) const  //are the site and its 8 neighbours in the plane free
    {
        return (unsigned)z< Nz && is_free(x-1,y-1,z) && is_free(x-1,y,z) && is_free(x-1,y+1,z) && is_free(x,y-1,z) && is_free(x,y,z) && is_free(x,y+1,z) && is_free(x+1,y-1,z) && is_free(x+1,y,z) && is_free(x+1,y+1,z);
    }


};



class Polymer
{
public:
    Lattice& lattice;
    std::vector<lattice_index_type> monomer_loc; //monomer locations using single lattice index notation
    std::vector<std::array<int,3>> monomer_loc3; //monomer locations using three index notation
    std::vector<unsigned int> bridges; //0 if no bridge, the monomer index+1 of the monomer to which ith monomer is bridged
    unsigned int n_bridged_monomers;
    const std::vector<std::vector<unsigned int>> monomer_neighbours;//neighbouring monomers of each monomer, specifies the topology


    Polymer(Lattice& lattice,const std::vector<lattice_index_type> &initial_loc,const std::vector<std::vector<unsigned int>> &neighbours):lattice(lattice),monomer_loc(initial_loc),monomer_neighbours(neighbours)
    {
        for(unsigned int i=0; i<(unsigned int)monomer_loc.size(); ++i)
        {
            lattice.set_monomer(monomer_loc[i],i+1);
        }
        monomer_loc3.resize(monomer_loc.size());
        for(size_t i=0; i<monomer_loc.size();++i){
            monomer_loc3[i]=lattice.get_indices(monomer_loc[i]);
        }
        bridges.resize(monomer_loc.size());
        n_bridged_monomers=0;
    }

    void print(std::ofstream &out,const double t) const
    {
        std::stringstream buffer;

        buffer << t << '\t' << n_bridged_monomers<< '\t';
        for(auto const &x: monomer_loc)
        {
            buffer << x<< '\t';
        }
        buffer << '\n';
        out << buffer.str();
    }

    void print_bridges(std::ofstream &out) const
    {
        std::stringstream buffer;

        for(auto const &x: bridges)
        {
            buffer << x<< '\t';
        }
        buffer << '\n';
        out << buffer.str();
    }

    void print() const
    {
        std::stringstream buffer;
        for(auto const &x: monomer_loc)
        {
            buffer << x << '\t';
        }
        buffer << '\n';
        std::cout << buffer.str();

//        for(auto const &x: bridges)
//        {
//            std::cout << x<< '\t';
//        }
//        std::cout << '\n';
    }

protected:

private:
};

#endif // POLYMER_H






