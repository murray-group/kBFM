
load('polymer_library');

ind=1;
clear P;
for i=1:N
P(i,:)=get_coord(configs(ind,i));
end
x=P(:,1);
y=P(:,2);
z=P(:,3);

[X1,X2,dyz_free,X3,X4,dxz_free]=get_densities(configs,Nx,Ny,Nz);

dyz_free=(Ny-2)/(Nx-2)*dyz_free;%scale to match the prob. range

load('polymer_p20_bridging40_kun1');

ind=1001;
clear P;
for i=1:N
P(i,:)=get_coord(configs(ind,i));
end
x_b=P(:,1);
y_b=P(:,2);
z_b=P(:,3);

[X1,X2,dyz_bridged,X3,X4,dxz_bridged]=get_densities(configs,Nx,Ny,Nz);
dyz_bridged=(Ny-2)/(Nx-2)*dyz_bridged;%scale to match the prob. range

cmin=min([min(dyz_free(:)),min(dxz_free(:)),min(dyz_bridged(:)),min(dxz_bridged(:))]);
cmax=max([max(dyz_free(:)),max(dxz_free(:)),max(dyz_bridged(:)),max(dxz_bridged(:))]);

%%
figure(12)

c=parula(256);
h=colormapline(x,y,z,c);
set(h,'linewidth',7)
axis equal;
axis off;
% xlim([0,Nx-1])
% ylim([0,Ny-1])
% zlim([0,Nz-1])
box off;
grid off;
%ax=gca;
%ax.BoxStyle = 'full';
%set(gca,'XTickLabel',[]);
%set(gca,'YTickLabel',[]);
%set(gca,'ZTickLabel',[]);

hold on;
line([1,Nx],[1,1],[1,1],'Color','k')
line([1,Nx],[1,1],[Nz,Nz],'Color','k')
line([1,Nx],[Ny,Ny],[1,1],'Color','k')
line([1,Nx],[Ny,Ny],[Nz,Nz],'Color','k')

line([1,1],[1,1],[1,Nz],'Color','k')
line([1,1],[Ny,Ny],[1,Nz],'Color','k')
line([1,1],[1,Ny],[1,1],'Color','k')
line([1,1],[1,Ny],[Nz,Nz],'Color','k')

line([Nx,Nx],[1,1],[1,Nz],'Color','k')
line([Nx,Nx],[Ny,Ny],[1,Nz],'Color','k')
line([Nx,Nx],[1,Ny],[1,1],'Color','k')
line([Nx,Nx],[1,Ny],[Nz,Nz],'Color','k')

colormap jet(256)
caxis([cmin,cmax])
surf(X3,X4,-10*ones(Nz-2,Nx-2),dxz_free,'EdgeColor','none')
surf(100*ones(Nz-2,Ny-2),X1,X2,dyz_free,'EdgeColor','none')
hold off;
colorbar;

figure(13)

c=parula(256);
h=colormapline(x_b,y_b,z_b,c);
set(h,'linewidth',7)
axis equal;
axis off;
% xlim([0,Nx-1])
% ylim([0,Ny-1])
% zlim([0,Nz-1])
box off;
grid off;
%ax=gca;
%ax.BoxStyle = 'full';
%set(gca,'XTickLabel',[]);
%set(gca,'YTickLabel',[]);
%set(gca,'ZTickLabel',[]);

hold on;
line([1,Nx],[1,1],[1,1],'Color','k')
line([1,Nx],[1,1],[Nz,Nz],'Color','k')
line([1,Nx],[Ny,Ny],[1,1],'Color','k')
line([1,Nx],[Ny,Ny],[Nz,Nz],'Color','k')

line([1,1],[1,1],[1,Nz],'Color','k')
line([1,1],[Ny,Ny],[1,Nz],'Color','k')
line([1,1],[1,Ny],[1,1],'Color','k')
line([1,1],[1,Ny],[Nz,Nz],'Color','k')

line([Nx,Nx],[1,1],[1,Nz],'Color','k')
line([Nx,Nx],[Ny,Ny],[1,Nz],'Color','k')
line([Nx,Nx],[1,Ny],[1,1],'Color','k')
line([Nx,Nx],[1,Ny],[Nz,Nz],'Color','k')

colormap jet(256)
caxis([cmin,cmax])
surf(X3,X4,-10*ones(Nz-2,Nx-2),dxz_bridged,'EdgeColor','none')
surf(100*ones(Nz-2,Ny-2),X1,X2,dyz_bridged,'EdgeColor','none')
hold off;
colorbar;

function [X1,X2,dyz,X3,X4,dxz]=get_densities(configs,Nx,Ny,Nz)
get_coord =@(ind) [floor(double(ind)/(Ny*Nz))+1;floor(mod(double(ind),Ny*Nz)/Nz)+1; mod(mod(double(ind),Ny*Nz),Nz)+1];

N=size(configs,2);
x=NaN(N*size(configs,1),1);
y=NaN(N*size(configs,1),1);
z=NaN(N*size(configs,1),1);
for ind=1:size(configs,1)
for i=1:N
P(i,:)=get_coord(configs(ind,i));
end
x(N*(ind-1)+[1:N])=P(:,1);
y(N*(ind-1)+[1:N])=P(:,2);
z(N*(ind-1)+[1:N])=P(:,3);

end

[X1,X2]=meshgrid(2:Ny-1,2:Nz-1);% no monomer can ever be at the edge, so exclude edges
f=ksdensity([y,z],[X1(:),X2(:)]);
dyz=reshape(f,Nz-2,Ny-2);

clear f;
clear xi;
[X3,X4]=meshgrid(2:Nx-1,2:Nz-1);
f=ksdensity([x,z],[X3(:),X4(:)]);
dxz=reshape(f,Nz-2,Nx-2);

end
