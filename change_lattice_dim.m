
p=load('input_87_87_87_linear.txt');
Nx=87;
Ny=87;
Nz=87;
get_coord =@(ind) [floor(double(ind)/(Ny*Nz))+1;floor(mod(double(ind),Ny*Nz)/Nz)+1; mod(mod(double(ind),Ny*Nz),Nz)+1];

P=get_coord(p);

Nnew=150;

P=P+round((Nnew-Nx)/2);

get_single_index =@(v) v(1)*Nnew*Nnew+v(2)*Nnew+v(3);

for i=1:length(p)
p(i)=uint32(get_single_index(P(:,i)));
end

writematrix(p,'input_150_150_150_linear.txt','Delimiter','tab')